%can be checked with http://www.optioneducation.net/calculator/main_advanced.asp
%info on american put are here : http://www.maths.usyd.edu.au/u/UG/SM/MATH3075/r/Slides_7_Binomial_Market_Model.pdf

S0 = 100;
T=0.75;
K=95;
sigma=0.2;
r=0.06;
n=25
deltaT=T/n;
u=exp(sigma*(deltaT)^0.5);
d=1/u;
p=((1+r)^deltaT-d)/(u-d);
europeanPutPrice = europeanPutPricing(S0, K, r, T, deltaT, n, u, d,p)

americanPutPrice = americanPutPricing(S0, K, r, T, deltaT, n, u, d,p)




