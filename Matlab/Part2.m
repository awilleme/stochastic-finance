clc;
clear all;
close all;
%========================
% GLOBAL PARAMETERS
%========================

T=0.5;
r_c=0.05;
r=log(1+r_c);
sigma=0.1237; %S&P500 1/04/17
S0=2362.75; %S&P500 1/04/17
K=0.95*S0;

M=1000000;
n=500;
dt=T/(n-1);
t=linspace(0,T,n);
%========================
% COMPUTING
%========================
% 1. Asset simulation
dw=normrnd(0,sqrt(dt),M,n-1);

S=zeros(M,n);
S(:,1)=S0;
for i=2:1:n
    S(:,i)=S(:,i-1) .* (1 + r*dt + sigma*dw(:,i-1));
end

% 2. Payoff
P=max(S(:,n).^2-K,0);

% 3. Expected value under risk neutral measure
T=exp(-r*T)*mean(P);
disp('=================================');
disp(strcat('Option value=',num2str(T)));
disp('=================================');

