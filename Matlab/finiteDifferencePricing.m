format long
S0 = 2362.75;
K=0.95*S0;
sigma=0.1237;	
rc=0.05;
r=log(1+rc);

N=1000000; %echantillonnage spatial
M=150; %echantillonnage du sous-jacent
T=0.5;
Smax=2.0*S0;

N=Narray(ni);
M=Marray(mi);
S=zeros(M+1);
C=zeros(N+1,M+1);
deltaT=T/N;
deltaS=Smax/M;
tmp=0:1:M;
S=tmp*deltaS;
C(N+1,:)=max(S.^2-K,0);
for i=N-1:-1:0
   for j=0:1:M
       a = 1/(1+r*deltaT) * (-0.5*r*j*deltaT + 0.5*sigma^2*j^2*deltaT);
       b = 1/(1+r*deltaT) * (1.0 - (sigma^2)*(j^2)*deltaT);
       c = 1/(1+r*deltaT) * (0.5*r*j*deltaT + 0.5*sigma^2*j^2*deltaT);
       iarray=i+1;
       jarray=j+1;
       C(iarray,jarray) = a * C(iarray+1,max(jarray-1,1)) + b * C(iarray+1,jarray) + c * C(iarray+1,min(jarray+1,M+1));
   end
end
S(M/2+1);
tarif = C(1,M/2+1);
