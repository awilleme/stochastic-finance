function optionPrice = americanPutPricing(S, K, r, T, deltaT, n, u, d,p)
    n=n-1;
    if n == 0 %if leaf of the tree
        payoffUp=max(0,K-(S*u));
        payoffDown=max(0,K-(S*d));
    else %if node of the tree
        payoffUp=americanPutPricing(S*u,K,r,T-deltaT,deltaT,n,u,d,p);
        payoffDown=americanPutPricing(S*d,K,r,T-deltaT,deltaT,n,u,d,p);
    end 
    optionPrice=max((K-S),1/(1+r)^deltaT * (p*payoffUp+(1-p)*payoffDown));
end

