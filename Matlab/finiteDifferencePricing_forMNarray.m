%can be checked with http://www.optioneducation.net/calculator/main_advanced.asp
%info on american put are here : http://www.maths.usyd.edu.au/u/UG/SM/MATH3075/r/Slides_7_Binomial_Market_Model.pdf
format long
S0 = 2362.75;
K=0.95*S0;
sigma=0.1237;	
rc=0.05;
r=log(1+rc);

%Narray = [10 100 1000 10000 100000 1000000];
%Narray = [10000 50000 100000 500000 1000000];
Narray=[500];
Marray=[500]; %nombre d'échantillons considérés entre Smax et 0
T=0.5;
Smax=2.0*S0;
tarif = zeros(size(Narray,2),size(Marray,2));
for ni=1:1:size(Narray,2)
    for mi=1:1:size(Marray,2)
        N=Narray(ni);
        M=Marray(mi);
        S=zeros(M+1);
        C=zeros(N+1,M+1);
        deltaT=T/N;
        deltaS=Smax/M;
        tmp=0:1:M;
        S=tmp*deltaS;
        C(N+1,:)=max(S.^2-K,0);
        for i=N-1:-1:0
           for j=0:1:M
               a = 1/(1+r*deltaT) * (-0.5*r*j*deltaT + 0.5*sigma^2*j^2*deltaT);
               b = 1/(1+r*deltaT) * (1.0 - (sigma^2)*(j^2)*deltaT);
               c = 1/(1+r*deltaT) * (0.5*r*j*deltaT + 0.5*sigma^2*j^2*deltaT);
               iarray=i+1;
               jarray=j+1;
               C(iarray,jarray) = a * C(iarray+1,max(jarray-1,1)) + b * C(iarray+1,jarray) + c * C(iarray+1,min(jarray+1,M+1));
           end
        end
        S(M/2+1);
        tarif(ni,mi) = C(1,M/2+1);
    end
end

semilogx(Narray,tarif,'o-');
yt=get(gca,'YTick');
ylab=num2str(yt(:), '%.2f');
set(gca,'YTickLabel',ylab);
xlabel('Log10 de N');
ylabel('Valeur de l option');
