﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace putPricing
{
    class Program
    {
        static double europeanPutPricing(double S, double K, double r, double T, double deltaT, double n, double u, double d, double p)
        {
            n=n-1;
            double payoffUp, payoffDown, optionPrice;
            if (n == 0)
            { //if leaf of the tree
                payoffUp = Math.Max(0, K - (S * u));
                payoffDown = Math.Max(0, K - (S * d));
            }
            else
            { //if node of the tree
                payoffUp = europeanPutPricing(S * u, K, r, T - deltaT, deltaT, n, u, d, p);
                payoffDown = europeanPutPricing(S * d, K, r, T - deltaT, deltaT, n, u, d, p);
            }
            optionPrice=1/Math.Pow((1+r),deltaT) * (p*payoffUp+(1-p)*payoffDown);
            return optionPrice;
        }
        static double americanPutPricing(double S, double K, double r, double T, double deltaT, double n, double u, double d, double p){
            n=n-1;
            double payoffUp, payoffDown, optionPrice;
            if (n == 0)
            { //if leaf of the tree
                payoffUp = Math.Max(0, K - (S * u));
                payoffDown = Math.Max(0, K - (S * d));
            }
            else
            {//if node of the tree
                payoffUp = americanPutPricing(S * u, K, r, T - deltaT, deltaT, n, u, d, p);
                payoffDown = americanPutPricing(S * d, K, r, T - deltaT, deltaT, n, u, d, p);
            }
            optionPrice=Math.Max((K-S),1/Math.Pow((1+r),deltaT) * (p*payoffUp+(1-p)*payoffDown));
            return optionPrice;
        }
    

        static void Main(string[] args)
        {
            Console.WriteLine("American and European put pricer, push enter to continue");
            Console.ReadLine();
            double S0 = 100;
            double T=0.75;
            double K=95;
            double sigma=0.2;
            double r=0.06;
            double[] n = new double[] {1, 2, 3, 5, 8, 10, 12, 15, 18, 20, 22, 25, 28, 30, 32, 35, 38, 40};
            int q;
            for(q=0;q<n.Length;q++)
            {
                double deltaT = T / n[q];
                double u = Math.Exp(sigma * Math.Pow((deltaT), 0.5));
                double d = 1 / u;
                double p = (Math.Pow((1 + r), deltaT) - d) / (u - d);
                Console.WriteLine("S0 : " + S0 + " K : " + K + " r : " + r + " T : " + T + " deltaT : " + deltaT + " n : " + n[q] + " u : " + u + " d : " + d + " p : " + p);
                double europeanPutPrice = europeanPutPricing(S0, K, r, T, deltaT, n[q], u, d, p);
                Console.WriteLine("N = "+n[q]+" European put price :"+europeanPutPrice);
                double americanPutPrice = americanPutPricing(S0, K, r, T, deltaT, n[q], u, d, p);
                Console.WriteLine("N = " + n[q] + " American put price :" + americanPutPrice);
            }
            Console.WriteLine("Fini");
            Console.ReadLine();
            Console.ReadLine();
            Console.ReadLine();


        }
    }
}
